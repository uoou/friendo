---
date: 12 Dec 2022
title: Small internet rambles also gopher
---

The influx of new users on the fediverse and all the comments about how it feels more like the internet of the early 00s (in a good way - more human, weirder, more idiosyncratic and *friendlier*) put me in a reminiscing mood.

I thought it might be fun to rewrite my website in HTML 3.2 (the last HTML before CSS). I'm not sure that *would* be fun, but the simplicity is enticing. Anyone - *really anyone* - could make a website, back in the day, that was as good as most other websites. A web page back then was just a text document with headings and hyperlinks and the odd in-line image. They all looked pretty much the same, just varying colours and, later on, background images.

CSS introduced just enough complexity to put web pages beyond the reach of the less technical. Its use also required a whole new set of design skills. Compared to the simple text-markup of early HTML, making a page with HTML and CSS was a relatively laborious and skill intensive task. Typing up a document vs. laying out a magazine.

You can make a web page without CSS still, of course. You can make a page in HTML 3.2 and it'll still work. But it'll look different from 'professional' websites. Its 'amateurishness' will make it stand out. The bar to access the 'proper' web - the web that looks how people expect the web to look - is set pretty high.

I like those amateur pages that look like a human made them rather than a corporation, but that's kinda beside the point. To an audience whose expectations are increasingly set by slick corporate sites, they look... 'inferior' isn't quite right, more like inauthentic - like cheap knock-offs.

The corporate solution to this is everyone gets a Facebook page or a Medium... thing or, slightly better, a Wordpress site. But that's so *dulllll*.

> down with bourgeois fairy-tale scenarios... long live life as it is!

I'm taking that Dziga Vertov quote waayyyy out of context, he was talking about dramatic movies. But the small internet is not a million miles away from the *Kino Eye* he was proposing. Life caught unawares, without the theatre that social media imposes.

I read a thread on Mastodon the other day, some retro-computing people complaining about websites that disable http and insist on https meaning that old and low-powered devices can't access those sites - the encryption involved is too taxing. The same is true, sadly, for Gemini, which insists on TLS authentication.

It seems to me that we should really be valuing low power use over everything else. Whether in an effort to avoid climate catastrophe or as preparation for the post apocalyptic hellscape when we fail. I'll want to read those 'how to farm' gemini pods on my ethanol-powered single-board-computer or scavenged c64.

And just because it's cool. The corporate web with its maximalism feels like someone's just learned to use a video editor and uses *alll* the transitions and effects *allll* the time. It's heavy, ugly and increasingly exhausting to use. The small web, Gemini and Fedi feel light, fun, exciting and, most of all, *human*.

Here's a cool thing:

[http://solarprotocol.net/](http://solarprotocol.net/)

> This website is hosted across a network of solar powered servers and is sent to you from whichever server is in the most sunshine.

That's so much more fun and exciting than anything Google, Meta or Twitter have done for about a million years.

This is getting increasingly rambly. These are all vaguely connected thoughts that have been floating around in my head for a bit. I guess this is me trying and mostly failing to connect them up.

The upshot is: I love the small internet and the wackos who make stuff on it. Thank you all.

Also I'm on Gopher now, same url.
