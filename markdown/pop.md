---
date: 2021-01-14
title: Useful script : pop
---

I used to use [Popcorntime](https://popcorntime.app/) a lot but always found the interface slow and cumbersome, especially when I knew exactly what I wanted to watch. What I *really* wanted was a <abbr title="Command Line Interface">CLI</abbr> program where I could just type the name of the thing I wanted to watch and then have it open up in [mpv](https://mpv.io/).

So I made it. [Here](https://gitlab.com/uoou/dotfiles/-/blob/master/stow/bin/home/drew/.local/bin/pop) it is.

It's not a particularly clever script, it just plugs together a couple of other clever things. But that's what bash scripting's all about, innit.

It requires `peerflix`, `jq` and `mpv` (you can make it work with another player by looking at peerflix's help and then editing line 15 of the script).

It's pretty simple to use. For films you give it a title and optionally a resolution. For TV shows you give it a title, an episode and optionally a resolution. e.g.:

`pop "white men can't jump"`

or

`pop "white men can't jump" 1080`

and for TV shows...

`pop "the expanse" 5 7`

or 

`pop "the expanse" 5 7 720`

To watch Season 5, Episode 7.

If you omit the resolution it *should* default to the best-seeded torrent, but that's unreliable in my experience.

So long as what you put in the ""s for the title is the top result on an IMDB search, and a torrent exists in Popcorn's database, then after a few seconds it should just start playing.

Here's what it looks like in action...

<video controls width="100%">
  <source src="../../video/pop.mp4" type="video/mp4">
</video>

### But how do it works?

It's not very sophisticated ‒ it scrapes an IMDB web search for the top result, gets the IMDB id, passes that to the Popcorn API and then processes the results with `jq`.

[Peerflix](https://github.com/mafintosh/peerflix) does the clever bit ‒ taking the resulting magnet link and streaming it in a way that a video player can handle.

All my script really does is make the searching for a magnet link easier.

Speaking of which, if you just want `pop` to spit out a magnet link to use for purposes other than watching the media right now, you can do:

`POP_ECHO=y pop "night of the living dead"`

I think that's about it. I hope someone finds it useful!

