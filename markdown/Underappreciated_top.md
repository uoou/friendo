---
date: 2019-11-10
---

There are some fancy process monitoring <abbr title="Terminal User Interface">TUI</abbr> programs these days. Aside from the venerable [htop](https://hisham.hm/htop/) there are also upstarts like [glances](https://github.com/nicolargo/glances) and [vtop](https://github.com/MrRio/vtop) and numerous others.

But what about poor old neglected top? It may look like a bag of trowels out-of-the-box, but with a little care and attention it can look pretty cool, too. Here's mine:

![](/images/top_top.png)

And it does it without needing a huge scripting language to run; its only real dependency is `ncurses` and you can pretty much guarantee it'll be preinstalled on any machine you use, regardless of the flavour of Unix it's running.

And being a proper Unixy thing, with top you can do things like:

```
top | grep firefox
```

Can those fancy monitors do that? Can they fuck as like.

### But how can I make *my* top look *fly* like *yours*?

Well, I'm glad you asked.

Top's documentation is *excellent* -- both the manpage and the in-application help which is accessed by pressing `h`. If you enjoy figuring stuff out yourself then the latter is all you really need -- you can close this right now, press `h` and get customising.

At any time while customising your top you can press `W` to save your settings.

Now let's get some nice CPU bars. Press `1` to expand the CPU info thing at the top to show all cores. The default is to show text-based stats so press `t` to get pretty bars. You can press `t` a few more times if you'd like to see your other options.

You can do the same with the memory usage summary by pressing `m` -- cycle through the options and use what you prefer. Personally, for memory, I prefer the full stats.

Press `0` to omit numerical fields with zeroed values from the process list if you think that looks nicer/clearer as I do. Then `J` to justify numbers.

If you only want to show tasks for your user, type `u` and then enter your username.

Now press `z` to get some colour and `x` to highlight the sorted column with bold.

Some optional stuff:

* Press `c` to toggle between just task names and full paths
* Press `e` and `E` to cycle through scales of memory units -- i.e. bytes, KiB, MiB etc. -- in the task list and memory area respectively.
* Press `V` to view processes as a tree

Okay, so that's a good start. All we've done so far is press `1t0Jzx` and now we have this:

![](/images/top_1t0Jzx.png)

Press `W` to save your *hard work*.

### Windows‽

Top's *big secret* is that it comes with four *windows* defined (that's what it calls them). By default you're in *Single Window Mode* -- Press `A` to switch to *Multiple Window Mode*.

Unfortunately these new windows are *ugly*. We can switch between windows by pressing `a` and `w`, so let's pretty them up by pressing `a1t0Jzx` three times.

Now we have four windows each with the same settings, like so:

![](/images/top_a1t0Jzx_x_3.png)

You can use as many of these four windows as you like, each with its own fields and settings. I like to have two -- one ordered by CPU usage and the other by memory. Adjust to taste, obviously.

First of all let's hide the bottom two windows. Move to them using `a` and `w` and press `-` to hide them. you can tell what window is currently selected by looking at the number in the very top left (if you don't see that, press `l` to being the summary back).

You can also switch windows by pressing `g` then the number of the window you want to select.

Press `g1` to select the first window then `G` and name this window as you please. Now press `f`.

![](/images/top_fields.png)

This screen allows you to customise which fields are displayed in the currently selected window. Use cursor keys (or `Alt` + `j` and `k`) to move up and down this list, `d` to toggle whether a field is displayed and `s` to choose the field by which the window is sorted.

To move fields around, press right cursor (or `Alt` + `l`) to select an item for move, then just move it up and down the list as you like. Press left cursor (or `Alt` + `h`) or `Enter` to complete the move.

Press `q` when you're done.

Now press `Z` so we can make the colours pretty.

![](/images/top_colours.png)

This window explains itself very well so take some time to read it. At the top is a preview of how the selected window will look. Press the keys `0` - `7` to change the colour of the currently selected element. Select an element by pressing `S`, `M`, `H` and `T` for *Summary Data*, *Message Prompts*, *Column Heads* and *Task Information* respectively.

Colour all the things until they sparkle then press `Enter` to commit your changes and return to top. Be careful here, unlike on the previous screen pressing `q` will abort your changes.

Repeat these steps until the windows you want to use have all the fields and colours you want.

And you're done!

Don't forget to press `W` to save your customisations!

### Now my top looks *dope* but how do I actually *use* it?

Top can do a lot of clever stuff but let's be honest, as desktop users all we really want a process monitor for is generally keeping an eye on things and killing anything that misbehaves. If you're a sysadmin you no doubt know what you're doing much better than I do.

So I'll just explain how to filter your process list and kill anything that's being naughty.

First of all, select the window that best represents what you're interested in (i.e. CPU usage, RAM usage etc.) then press `A` to make things clearer.

To filter this list press `o` (or `O` if you wish your filter to be case-sensitive) then type, for example:

* `COMMAND=firefox` to show processes whose COMMAND field contains `firefox`
* `!COMMAND=firefox` to show processes which *don't* contain `firefox`
* `%CPU>3.0` to filter to processes whose %CPU field is greater than 3.0

For more info on filters do a `man top`.

Okay, let's clear those filters by pressing `=`.

To kill a task you simply press `k` then type the PID of the process you wish to kill. This will default to the process at the top of the current list, which is usually what you want -- i.e. if you wanted to kill the process with the highest CPU use, you'd choose your CPU usage window and then press `k`. If that's not the case then filtering, as described above, can make it easier to spot the PID of the process you wish to kill.

After choosing a PID you'll be asked which signal to send. The default is SIGTERM (signal number 15) which tells the process to exit in a seemly manner. If you want to send the "fuck you, close" SIGKILL instead, [bad practise](https://bencane.com/2014/04/01/understanding-the-kill-command-and-how-to-terminate-processes-in-linux/) as it may be, enter `9`.

And that's it. Top may not be the prettiest of all the task managers but it's *extremely* powerful and customisable and, to my mind, just looks and feels more *unixy* and thus, obviously, *cooler*...


<div class="ytvid">
<iframe class="video" src='https://www.youtube.com/embed/Bmz67ErIRa4' frameborder='0' allowfullscreen=""></iframe>
</div>

