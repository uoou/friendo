---
date: 2019-10-11
---

I've not had a personal website for a looooong time.

[Hex](http://hexdsl.co.uk/) asked me a while back to make a simple, bash based static site generator. Months later, I did as asked and made [blop](https://gitlab.com/uoou/blop) and, since I'd made it, I thought I should really use it.

I've long admired [Chris Were's site](https://chriswere.uk/) for its authenticity and simplicity. It reminds me of the sites I'd find during my early experience of the web — deeply idiosyncratic sites where *real* people would talk freely about whatever interests them, without the goldfish-bowl qualities of later silos like Facebook and Twitter and that.

So it's been brewing for a while, I'd wanted my own internet house for some time and reading [this post](https://www.jvt.me/posts/2019/07/22/why-website/), linked on Mastodon yesterday, pushed me over the edge.

This in particular struck a chord:

>the World Wide Web is this amazing thing that was literally **built for everyone**. We need to make sure that we are all using it to its best, and owning a piece of it to show big companies that it's ours, not theirs!

Yeeeeeeeeeeah.

Fuck you {Facebook, Twitter, Google}. Time to take back control of the ~~streets~~ web.

