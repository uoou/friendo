---
date: 2020-02-03
title: Easier ssh with .ssh config
---

I didn't know about the `~/.ssh/config` file until quite recently and it's *really* handy, so I thought I'd share.

`~/.ssh/config` lets you make what I think of as ssh aliases. You can give connections short names and specify various connection settings ‒ pretty much anything you can pass to `ssh`'s <abbr title="Command Line Interface">CLI</abbr> can be configured here.

Here's a simple example:

```
Host nas
	Hostname 192.168.1.10

Host laptop
	Hostname 192.168.1.11
	User laptop-me

Host someserver
	Hostname 203.0.113.1
	User seriousthings
	Port 10100
	PubkeyAuthentication yes
	Identityfile ~/.ssh/id_rsa_seriousthings

Host *
	PubkeyAuthentication no
```

I am then able to do, for example, simply:

```
ssh nas
```

These settings apply from top to bottom, cumulatively. So you want to structure this file with **specific rules above general rules**, otherwise the general rules will override the specific, which is not what you want.

One nice benefit is that you'll get autocompletion of hosts defined in `~/.ssh/config` when using `ssh` under bash or zsh (at least).

`~/.ssh/config` can also be used to shore up some of the leaky parts of sshing, as described [here](https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-ssh-config/#gfm-10) ‒ the whole page, and other articles in the series (linked at the top) are worth a read if you use ssh a decent amount.



