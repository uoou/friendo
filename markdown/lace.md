---
title: A script to interleave tiny Gemini logs
date: 2021-02-18
---

I guess what this script is, really, is a client-of-sorts for Gemini tiny logs. Which is kinda like micro-blogging on Gemini.

*Client* is overstating it though. It's written in the laziest, simplest way I can think of and it will collapse if the content is not as it expects.

Let's call it a first pass.

[https://gitlab.com/uoou/dotfiles/-/tree/master/stow/bin/home/drew/.local/bin/lace](https://gitlab.com/uoou/dotfiles/-/tree/master/stow/bin/home/drew/.local/bin/lace)

If you want to give it a go, just

```
wget https://gitlab.com/uoou/dotfiles/-/raw/master/stow/bin/home/drew/.local/bin/lace
chmod +x lace
```

And then either put it somewhere in your `$PATH` or call it with `./lace`.

Here's what it looks like ***in action***...

![](../../images/lace.png)

## How do it works?

To add a subscription to a tiny log, just do

```
lace sub nickname url
```

For example

```
lace sub Drew gemini://friendo.monster/tiny.gmi
```

To remove a subscription

```
lace unsub nickname
```

For example

```
lace unsub Drew
```

To list your subscriptions

```
lace subs
```

And to read your subscriptions, just do

```
lace
```

Or, if you want it to output straight to the CLI rather than to `less`

```
lace nope
```

You can do

```
lace help
```

For a reminder of all this stuff.

## Formatting expectations

`lace` expects tiny logs to be formatted in a particular way.

You can have whatever you want at the top - a level 1 heading and an introductory paragraph or whatever, that's all fine.

Then entries must consist of a date in a format that `date -d` can understand as input as a level 2 heading and then a single newline and then text with no double newlines (i.e. no paragraph breaks. Single line breaks are fine) and no links. I'm not happy about there being no links and intend to fix that later.

Entries should be separated by double newlines.

The date in the level 2 heading should include a timezone, otherwise `lace` will assume that the log was written in whatever timezone it's running under, which will often not be the case and will thus result in the reported time of log entires being *lies*.

For example, this is a tiny log that would work with `lace`

```
# My tiny log

Welcome to my tiny log. This is where I will write small things for small reading.

## Thu 18 Feb 2021 17:22 GMT
Looking forward to Thabletop Thursday tonight!

## Wed 17 Feb 2021 14:23 GMT
Ate too many fajitas.

=> ..
```

The format I use for the date there is

```
date +'%a %d %b %Y %H:%M %Z'
```

And when I say the date must be understandable by `date -d`, what I mean is that if you take the date format you're using and do this

```
date -d "Thu 18 Feb 2021 17:22 GMT"
```

The output will be a date, rather than `date: invalid date`.

This is the script I use for generating my tiny logs. If you use this (with the paths and the host in the rsync command altered to your needs, of course) then `lace` should handle your tiny log just fine.

```
#!/usr/bin/env bash

# https://gitlab.com/uoou/dotfiles/-/tree/master/stow/bin/home/drew/.local/bin/tiny

[ -z "$1" ] && exit

sed -i "3i## $(date +'%a %d %b %Y %H:%M %Z')\n$*\n" ~/Documents/friendo_gemini/tiny.gmi &&
rsync -aP ~/Documents/friendo_gemini/tiny.gmi mouse:~/friendo_gemini/tiny.gmi
```

[https://gitlab.com/uoou/dotfiles/-/blob/master/stow/bin/home/drew/.local/bin/tiny](https://gitlab.com/uoou/dotfiles/-/blob/master/stow/bin/home/drew/.local/bin/tiny)
