---
date: 2021-02-03
title: There is no right Linux distro
---

The question "What's the best Linux distro for me?" or "What's the best Linux distro for X?" (Where "X" is "playing DVDs" or "coding" or "playing games" or any number of other specific tasks) is often asked on reddit, forums and other discussion places, usually asked by a new user trying to navigate the confusing landscape of hundreds of available distros.

The right answer is: *Whichever one you like the most*. It's never the answer they're looking for, but it is the truth. 

## Linux is Linux

The differences between distros are pretty minor. With very few exceptions, all distros have the same kernel, the same core utilities, the same C library, the same init system and the *exact same applications*. Different versions of those things, perhaps, but it's essentially the same *stuff*.

And the areas where distros *do* differ:

* package manager
* community vs. corporate backed
* software availability
* stable vs. cutting edge
* free software purity

to name a few, aren't the sort of things that a new user is equipped to judge.

And that's really the point...

## I am not you

No matter how exhaustive a list of distro requirements you give me, I can all but guarantee that the reason you ultimately end up choosing one distro over another wasn't on that list.

The reasons we really end up settling on our distro of choice, if we ever do, tend to be idiosyncratic and, really, pretty esoteric at times. And by *esoteric* I might just mean *dumb*. "I prefer how they format package names" or "they have a cool logo" or "red is better than blue".

Choosing a particular Linux distro isn't like choosing, say, a graphics card, where you can just weigh up features to price and come up with a pretty rational choice.

Differences in distros are more like differing personalities than feature differences and, since there's (most often) no price, it's more like an infinitely varied ecosystem than a consumer choice. And I mean an actual ecosystem ‒ it's like dumping someone in a wildflower meadow and saying: Which flower is the prettiest?

## There's no short-cut

You can give us some coarsely-grained preferences and we can recommend a handful of distros that satisfy them, but then you have to go and try out some distros. I guarantee that when you do, you'll come up with preferences that weren't anywhere near being on your list. Because that's how it works ‒ they emerge through use and familiarity.

The best disro for you is the one you like the most. And the only way you're going to find it is by trying a bunch of them out. 

It's laborious and I'm sorry about that. But this isn't a transaction, you're not buying a product. You're choosing the prettiest flower and, to do that, you've got to live with them for a bit.

